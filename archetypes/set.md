---
title: '[{{ index (split .Name "-") 0 | upper }}] {{ delimit (after 1 (split .Name "-")) " " | title }}'
date: {{ .Date }}
#playdate: {{ .Date }}
artists: ['{{ index (split .Name "-") 1 }}']
events: ['{{ index (split .Name "-") 0 }}']
genres: []
draft: false
---

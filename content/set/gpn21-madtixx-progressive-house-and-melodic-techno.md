---
title: "[gpn21] MadTiXx - Progressive House & Melodic Techno"
date: 2024-01-28T13:45:16+01:00
playdate: 2023-06-08T22:30:00+02:00
artists: ['MadTiXx']
events: ['gpn21']
genres: ['Progressive House', 'Melodic House', 'Indie Dance', 'Alternative Dance', 'Techno']
draft: false
# alternative link: https://www.youtube.com/watch?v=VqEV7Z7RS9w
---

https://www.mixcloud.com/madtixx/progressive-house-melodic-techno-madtixx-gpn21-lounge-2023-06-08/

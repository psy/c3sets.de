---
title: "[eh21] DJ Sordid"
date: 2024-04-02T10:43:13+02:00
playdate: 2024-04-01T00:00:00+02:00
artists: ['DJ Sordid']
events: ['eh21']
genres: ['Electronic Body Music', 'Industrial', 'Rhythm and Noise']
draft: false
---

https://www.mixcloud.com/sordid/eh21-lounge/

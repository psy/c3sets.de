---
title: "[gpn21] Arpeggigator"
date: 2024-01-28T13:43:34+01:00
playdate: 2023-06-09T18:45:00+01:00
artists: ['Arpeggigator']
events: ['gpn21']
genres: ['Dawless', 'Live Electronics', 'Dub', 'Electro']
draft: false
# alternative link: https://www.youtube.com/watch?v=O6w2riVVu9I
---

https://www.mixcloud.com/arpeggigator/arpeggigator-gpn21-kuechenstage/

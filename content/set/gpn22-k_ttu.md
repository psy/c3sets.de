---
title: '[GPN22] k_ttu - lisää bassoa'
date: 2024-08-24T12:08:12+02:00
playdate: 2024-05-31T23:30:00+02:00
artists: ['k_ttu']
events: ['gpn22']
genres: ['UK Bass', 'Dubstep', 'Funk Carioca', 'Baile Funk', 'Dark Techno', 'Breakbeat']
draft: false
# alternative link: https://www.mixcloud.com/k_ttu/lis%C3%A4%C3%A4-bassoa-live-at-gpn22/
---

https://www.youtube.com/watch?v=osOv9TgfSeI

---
title: "[gpn20] Nervengift - Computers Are In Control live @ GPN20"
date: 2022-05-31T12:15:16+02:00
artists: ['nervengift']
events: ['gpn20']
genres: ['Analog', 'Electronica']
draft: false
# alternative link: https://www.mixcloud.com/LoungeControl/nervengift-at-gpn20-lounge-performance/
# alternative link: https://www.house-mixes.com/profile/LoungeControl/loungecontrol_nervengift-at-gpn20-lounge-performance
---

https://soundcloud.com/nervengiftlabs/computers-are-in-control-live-gpn20

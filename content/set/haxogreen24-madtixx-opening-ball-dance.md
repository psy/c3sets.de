---
title: "[Haxogreen24] MadTiXx - Opening Ball Dance"
date: 2024-07-31T14:52:24+02:00
playdate: 2024-07-24T21:00:00+02:00
artists: ['MadTiXx']
events: ['haxogreen24']
genres: ['Eurodance', 'Harddance', 'Techno']
draft: false
# alternative url: https://www.mixcloud.com/madtixx/opening-ball-dance-madtixx-haxogreen-2024-eurotrance-harddance-hardtechno/
---

https://www.youtube.com/watch?v=qmBZHlyUHYc